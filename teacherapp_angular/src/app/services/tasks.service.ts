import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import {Task } from '../models/Task';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TasksService {
  taskCollection: AngularFirestoreCollection<Task>;
  tasks: Observable<Task[]>;
  af: AngularFirestore;


  constructor(public afs: AngularFirestore) {
    
   }

   updateTasks(task: Task, day:string, tasks: string[]){
     this.af.collection('Tasks').add({
       day:day,
       tasks:tasks
     })
     .then(function(docRef) {
      console.log("Document written with ID: ", docRef.id);
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
  });
   }
}

