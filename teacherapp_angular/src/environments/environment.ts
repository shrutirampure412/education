// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBFIHnu1WPMas8PqtTDHX6Fpm-MjuC-Ck8",
    authDomain: "shiksha-da9f8.firebaseapp.com",
    databaseURL: "https://shiksha-da9f8.firebaseio.com",
    projectId: "shiksha-da9f8",
    storageBucket: "shiksha-da9f8.appspot.com",
    messagingSenderId: "1025734136947",
    appId: "1:1025734136947:web:c14b3ffffc75da2410c606",
    measurementId: "G-Q4EZMQGWF2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
