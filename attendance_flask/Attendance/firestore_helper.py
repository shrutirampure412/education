from firebase_admin import credentials, firestore
import firebase_admin


class FirestoreHelper:
    def __init__(self, creds_path):
        self.__cred = credentials.Certificate(creds_path)
        self.default_app = firebase_admin.initialize_app(self.__cred)
        self.db = firestore.client()

    def get_roll_nos(self, names):
        l = []
        for name in names:
            docs = self.db.collection(u'Students').where(u'name', u'==', name).stream()
            for doc in docs:
                print(f'{doc.id} => {doc.to_dict()}')
                l.append(doc.to_dict().get('sid'))
        return l

    def update_attendance_db(self, roll_nos, details):
        details['attendance'] = {}
        for i in range(20):
            sid = details['class'] + str(i)
            if sid in roll_nos:
                details['attendance'][sid] = True
            else:
                details['attendance'][sid] = False
        self.db.collection(u'Attendance').add(details)

    def get_attendance(self):
        sid = self.get_sid()[0]
        docs = self.db.collection(u'Students').where(u'sid', u'==', sid).stream()
        student_details = {'sid': sid}
        for doc in docs:
            print(f'{doc.id} => {doc.to_dict()}')
            student_details['name'] = doc.to_dict()['name']
            student_details['class'] = doc.to_dict()['class']

        date = "10-26-2020"
        docs = self.db.collection(u'Attendance').where(u'date', u'==', date).stream()
        att = 'Attendance:  '
        for doc in docs:
            a = doc.to_dict()
            att += a['subject'] + '- ' + ('attended' if a['attendance'][sid] else 'not attended') + ', '

        msg = "Dear parent, this is the report for " + student_details['name'] + "\n" + att + "\nScore in today's Maths test: 42/50"
        return msg


    def get_sid(self):
        doc_ref = self.db.collection(u'Parents').document(u'model')
        docs = self.db.collection(u'Parents').where(u'name', u'==', u'Suresh Gupta').stream()
        l = []
        for doc in docs:
            print(f'{doc.id} => {doc.to_dict()}')
            l.append(doc.to_dict().get('sid'))
        return l
