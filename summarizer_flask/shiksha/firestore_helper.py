from firebase_admin import credentials, firestore
import firebase_admin


class FirestoreHelper:
    def __init__(self, creds_path):
        self.__cred = credentials.Certificate(creds_path)
        self.default_app = firebase_admin.initialize_app(self.__cred)
        self.db = firestore.client()

    def update_summary(self, summary):
        self.db.collection('summaries').document().set(
            {'summary': summary,
            'subject': 'physics',
            'class':'6a'}
        )
        print('db uodated')

    def update_counts(self, name, n_occupied, n_empty):
        self.db.collection('ParkingLot').document(name).update(
            {
                'occupied': n_occupied,
                'empty': n_empty
             }
        )
