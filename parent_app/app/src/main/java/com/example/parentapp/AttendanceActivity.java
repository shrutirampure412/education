package com.example.parentapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.HashMap;

public class AttendanceActivity extends AppCompatActivity {

    private String sid;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    final String TAG = "parents2 firebase";
    HashMap<String, Boolean> hm = new HashMap<String, Boolean>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        Intent intent = getIntent();
        sid = intent.getExtras().getString("sid");

        db.collection("Attendance")
                .whereEqualTo("date", "10-26-2020")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                HashMap<String, Boolean> att = (HashMap<String, Boolean>) document.get("attendance");
                                hm.put(document.getString("subject"), att.get(sid));
                            }
                            TextView att1TV = findViewById(R.id.att1);
                            TextView att2TV = findViewById(R.id.att2);
                            TextView att3TV = findViewById(R.id.att3);
                            //TextView att4TV = findViewById(R.id.att4);

                            int i = 1;
                            for(String s : hm.keySet()) {
                                String a = hm.get(s) ? "Attended" : "Not attended";
                                if(i==1) {
                                    att1TV.setText(s+": "+a);
                                }
                                else if(i==2) {
                                    att2TV.setText(s+": "+a);
                                }
                                else if(i==3) {
                                    att3TV.setText(s+": "+a);
                                }
                                i++;
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


    }
}