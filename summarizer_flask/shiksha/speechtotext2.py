#!/usr/bin/env python
"""
Simple Flask application to demonstrate the Google Speech API usage.
Install the requirements first:
`pip install SpeechRecognition flask`
Then just run this file, go to http://127.0.0.1:5000/
and upload an audio (or may be even video) file there, using the html form.
(I've tested it with a .wav file only - relying on Google here).
"""

import os
from flask import Flask, request, redirect, flash
from werkzeug.utils import secure_filename
from pydub import AudioSegment

import speech_recognition as sr

app = Flask(__name__)
UPLOAD_FOLDER = "./"
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER

# You have 50 free calls per day, after that you have to register somewhere
# around here probably https://cloud.google.com/speech-to-text/
#GOOGLE_SPEECH_API_KEY = "4b6a3b6cd73588082835ac1404e0b2b1d582b871"
GOOGLE_SPEECH_API_KEY = None

def stt(audiofile):
    audio = AudioSegment.from_wav(audiofile) 
  
    ''' 
    Step #1 - Slicing the audio file into smaller chunks. 
    '''
    # Length of the audiofile in milliseconds 
    n = len(audio) 
    
    # Variable to count the number of sliced chunks 
    counter = 1
    
    # Text file to write the recognized audio 
    fh = open("recognized.txt", "w+") 
    
    # Interval length at which to slice the audio file. 
    # If length is 22 seconds, and interval is 5 seconds, 
    # The chunks created will be: 
    # chunk1 : 0 - 5 seconds 
    # chunk2 : 5 - 10 seconds 
    # chunk3 : 10 - 15 seconds 
    # chunk4 : 15 - 20 seconds 
    # chunk5 : 20 - 22 seconds 
    interval = 5 * 1000
    
    # Length of audio to overlap.  
    # If length is 22 seconds, and interval is 5 seconds, 
    # With overlap as 1.5 seconds, 
    # The chunks created will be: 
    # chunk1 : 0 - 5 seconds 
    # chunk2 : 3.5 - 8.5 seconds 
    # chunk3 : 7 - 12 seconds 
    # chunk4 : 10.5 - 15.5 seconds 
    # chunk5 : 14 - 19.5 seconds 
    # chunk6 : 18 - 22 seconds 
    overlap = 1.5 * 1000
    
    # Initialize start and end seconds to 0 
    start = 0
    end = 0
    
    # Flag to keep track of end of file. 
    # When audio reaches its end, flag is set to 1 and we break 
    flag = 0
    
    # Iterate from 0 to end of the file, 
    # with increment = interval 
    for i in range(0, 2 * n, interval): 
        
        # During first iteration, 
        # start is 0, end is the interval 
        if i == 0: 
            start = 0
            end = interval 
    
        # All other iterations, 
        # start is the previous end - overlap 
        # end becomes end + interval 
        else: 
            start = end - overlap 
            end = start + interval  
    
        # When end becomes greater than the file length, 
        # end is set to the file length 
        # flag is set to 1 to indicate break. 
        if end >= n: 
            end = n 
            flag = 1
    
        # Storing audio file from the defined start to end 
        chunk = audio[start:end] 
    
        # Filename / Path to store the sliced audio 
        filename = 'chunk'+str(counter)+'.wav'
    
        # Store the sliced audio file to the defined path 
        chunk.export(filename, format ="wav") 
        # Print information about the current chunk 
        print("Processing chunk "+str(counter)+". Start = "
                            +str(start)+" end = "+str(end)) 
    
        # Increment counter for the next chunk 
        counter = counter + 1
        
        # Slicing of the audio file is done. 
        # Skip the below steps if there is some other usage 
        # for the sliced audio files. 
    
    
        ''' 
        Step #2 - Recognizing the chunk and writing to a file. 
        '''
    
        # Here, Google Speech Recognition is used 
        # to take each chunk and recognize the text in it. 
    
        # Specify the audio file to recognize 
    
        AUDIO_FILE = filename 
    
        # Initialize the recognizer 
        r = sr.Recognizer() 
    
        # Traverse the audio file and listen to the audio 
        with sr.AudioFile(AUDIO_FILE) as source: 
            audio_listened = r.listen(source) 
    
        # Try to recognize the listened audio 
        # And catch expections. 
        try:     
            rec = r.recognize_google(audio_listened) 
            
            # If recognized, write into the file. 
            fh.write(rec+" ") 
        
        # If google could not understand the audio 
        except sr.UnknownValueError: 
            print("Could not understand audio") 
    
        # If the results cannot be requested from Google. 
        # Probably an internet connection error. 
        except sr.RequestError as e: 
            print("Could not request results.") 
    
        # Check for flag. 
        # If flag is 1, end of the whole audio reached. 
        # Close the file and break. 
        if flag == 1: 
            fh.close() 
            break


@app.route("/", methods=["GET", "POST"])
def index():
    extra_line = ''
    if request.method == "POST":
        # Check if the post request has the file part.
        if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
        file = request.files["file"]
        # If user does not select file, browser also
        # submit an empty part without filename.
        if file.filename == "":
            flash("No selected file")
            return redirect(request.url)
        if file:
            # Speech Recognition stuff.
            # recognizer = sr.Recognizer()
            # audio_file = sr.AudioFile(file)
            # with audio_file as source:
            #     audio_data = recognizer.record(source, duration=60)
            # text = recognizer.recognize_google(
            #     audio_data, key=GOOGLE_SPEECH_API_KEY, language="en-US", show_all=True
            # )
            # extra_line = f'Your text: "{text}"'
            stt(file)

            # Saving the file.
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            file.save(filepath)
            extra_line += f"<br>File saved to {filepath}"

    return f"""
    <!doctype html>
    <title>Upload new File</title>
    {extra_line}
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <p/>
      <input type=submit value=Upload>
    </form>
    """


if __name__ == "__main__":
    app.run(debug=True, threaded=True)