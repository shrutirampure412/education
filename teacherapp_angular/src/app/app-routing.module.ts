import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { TodoComponent } from './components/todo/todo.component';

const routes: Routes = [
  {path: 'tasks',component: TodoComponent},
  {path: '', component: HomepageComponent},
  //{ path:  'emergency/:id', component:  EmergencyFormComponent},
  //{path: 'helpNeeded', component: EmergencyFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
