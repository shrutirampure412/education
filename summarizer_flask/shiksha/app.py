# import pyrebase
# from google.cloud import firestore

# import os
# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'D:\Downloads\gc-creds-flask.json'

# config = {
#     "apiKey": "AIzaSyBFIHnu1WPMas8PqtTDHX6Fpm-MjuC-Ck8",
#     "authDomain": "shiksha-da9f8.firebaseapp.com",
#     "databaseURL": "https://shiksha-da9f8.firebaseio.com",
#     "projectId": "shiksha-da9f8",
#     "storageBucket": "shiksha-da9f8.appspot.com",
#     "messagingSenderId": "1025734136947",
#     "appId": "1:1025734136947:web:905c4f064e88c62e10c606",
#     "measurementId": "G-G2CKE3DWX9"
# }

# firebase = pyrebase.initialize_app(config)

# db = firestore.Client()

# from flask import *

# app = Flask(__name__)

# @app.route('/', methods=['GET', 'POST'])
# def basic():
#     if request.method=='POST':
#         name= request.form['name']
#         data = {
#                 u'subject': u'physics',
#                 u'class': u'6a',
#                 u'summary': name
#             }
#     return render_template('index.html')

# # # Add a new doc in collection 'cities' with ID 'LA'
# # db.collection(u'summaries').document().set(data)

# if __name__ =='__main__':
#     app.run(debug=True)

from flask import Flask, request, render_template
import pandas as pd
from firestore_helper import FirestoreHelper
import os
from summarizer import Summarizer


app = Flask(__name__)

# Firebase Helper
fh = FirestoreHelper('./gc-creds-flask.json')

def summarize(content):
    model = Summarizer()
    result = model(content, min_length=20)
    summary = "".join(result)
    return summary


@app.route('/')
def hello_world():
    return 'Hello World! You are on the main page of Shiksha'

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # uploaded = True
        df = request.files.get('file')
        if (df.filename != ''):
            df.save(os.path.join('uploads/',df.filename))
            with open(os.path.join('uploads/',df.filename), "r") as f: 
                content = f.read() 
                summary = summarize(content)
            fh.update_summary(summary)
            print(summary)
        return render_template('index.html', uploaded=True, summary=summary)

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)