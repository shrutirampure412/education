package com.example.shiksha;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Path;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class TasksActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private int progressStatus = 10;
    private TextView progressText2;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressText2 = findViewById(R.id.progressText2);
//        progressBar.setProgress(0);
//        button = findViewById(R.id.button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) { q
//                progressStatus++;
//                progressBar.setProgress(progressStatus);
//            }
//        });


        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference docRef = db.collection("Tasks");
        final String TAG = "tasks firebase";
        db.collection("Tasks")
                .whereEqualTo("day", "Tuesday")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                final HashMap<String, Map<String, String>> hm = (HashMap<String, Map<String, String>>) document.get("tasks");
                                Log.d(TAG, hm.toString());
                                Log.d(TAG, hm.get("9-10").get("link"));

                                //task 1
                                TextView task1_text = findViewById(R.id.task1_text);
                                final LinearLayout task1_circle = findViewById(R.id.task1_circle);
                                LinearLayout task1 = findViewById(R.id.task1);
                                task1_text.setText("9am - 10am\n"+hm.get("9-10").get("name"));
                                task1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ImageView viewOne = findViewById(R.id.rocket);
                                        ObjectAnimator animation = ObjectAnimator.ofFloat(viewOne, "translationY", 200f);
                                        animation.setDuration(5000);
                                        animation.start();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(hm.get("9-10").get("link")));
                                                if (intent.resolveActivity(getPackageManager()) != null) {
                                                    startActivity(intent);
                                                }
                                                task1_circle.setBackgroundResource(R.drawable.circle_done);
                                                increaseProgress();
                                            }
                                        }, 5000);


                                    }
                                });

                                //task 2
                                TextView task2_text = findViewById(R.id.task2_text);
                                final LinearLayout task2_circle = findViewById(R.id.task2_circle);
                                LinearLayout task2 = findViewById(R.id.task2);
                                task2_text.setText("10am - 11am\n"+hm.get("10-11").get("name"));
                                task2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ImageView viewOne = findViewById(R.id.rocket);
                                        ObjectAnimator animation = ObjectAnimator.ofFloat(viewOne, "translationY", 650f);
                                        animation.setDuration(5000);
                                        animation.start();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(hm.get("10-11").get("link")));
                                                if (intent.resolveActivity(getPackageManager()) != null) {
                                                    startActivity(intent);
                                                }
                                                task2_circle.setBackgroundResource(R.drawable.circle_done);
                                                increaseProgress();
                                            }
                                        }, 5000);

                                    }
                                });

                                //task 3
                                TextView task3_text = findViewById(R.id.task3_text);
                                final LinearLayout task3_circle = findViewById(R.id.task3_circle);
                                LinearLayout task3 = findViewById(R.id.task3);
                                task3_text.setText("11am - 12pm\n"+hm.get("11-12").get("name"));
                                task3.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ImageView viewOne = findViewById(R.id.rocket);
                                        ObjectAnimator animation = ObjectAnimator.ofFloat(viewOne, "translationY", 1160f);
                                        animation.setDuration(5000);
                                        animation.start();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(hm.get("11-12").get("link")));
                                                if (intent.resolveActivity(getPackageManager()) != null) {
                                                    startActivity(intent);
                                                }
                                                task3_circle.setBackgroundResource(R.drawable.circle_done);
                                                increaseProgress();
                                            }
                                        }, 5000);

                                    }
                                });

                                //task 4
                                TextView task4_text = findViewById(R.id.task4_text);
                                final LinearLayout task4_circle = findViewById(R.id.task4_circle);
                                LinearLayout task4 = findViewById(R.id.task4);
                                task4_text.setText("2pm - 3pm\n"+hm.get("2-3").get("name"));
                                task4.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ImageView viewOne = findViewById(R.id.rocket);
                                        ObjectAnimator animation = ObjectAnimator.ofFloat(viewOne, "translationY", 1600f);
                                        animation.setDuration(5000);
                                        animation.start();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {

                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(hm.get("2-3").get("link")));
                                                if (intent.resolveActivity(getPackageManager()) != null) {
                                                    startActivity(intent);
                                                }
                                                task4_circle.setBackgroundResource(R.drawable.circle_done);
                                                increaseProgress();
                                            }
                                        }, 5000);

                                    }
                                });
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


    }

    public void increaseProgress() {
        progressStatus += 5;
        progressBar.setProgress(progressStatus);
        progressText2.setText(String.valueOf(progressStatus) + "/100");
    }
}