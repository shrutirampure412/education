from flask import Flask, request, render_template
import pandas as pd
from firestore_helper import FirestoreHelper
from twilio.rest import Client

app = Flask(__name__)

# Firebase Helper
fh = FirestoreHelper('./gc-creds-flask.json')


@app.route('/')
def hello_world():
    return 'Hello World! You are on the main page of Shiksha'

def upload_attendance(df, details):
    names = df['Name'].tolist()
    roll_nos = fh.get_roll_nos(names)
    fh.update_attendance_db(roll_nos, details)


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        # uploaded = True
        file = request.files['file']
        df = pd.read_csv(file)
        # cols = df.columns
        # print(cols[0], cols[1], cols[2])
        date = file.filename.split(' ')[1]
        class_name = request.form['class']
        subject = request.form['subject']
        details = {'date': date, 'class': class_name, 'subject': subject}
        # print(date)
        # print(class_name)
        upload_attendance(df, details)
        return render_template('upload.html', uploaded=True)

    return render_template('upload.html')

@app.route('/sendsms')
def send_sms():
    msg = fh.get_attendance()
    print(msg)
    account_sid = 'ACaec01a315d06f65a2efbb985ab3dc1fb'
    auth_token = '8397fd7cf08f91fd4f2127f477c43efd'
    client = Client(account_sid, auth_token)

    message = client.messages.create(
        body=msg,
        from_='+14159425810',
        to='+918169023113 '
    )

    print(message.sid)
    return 'SMS sent'

if __name__ == '__main__':
    app.run(debug=True)
