package com.example.parentapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class DetailsActivity extends AppCompatActivity {
    private String sid;
    private String className;
    private String studentName;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    final String TAG = "parents2 firebase";
    private TextView teachersTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        teachersTV = findViewById(R.id.teachers);
        Intent intent = getIntent();
        sid = intent.getExtras().getString("sid");

        //textviews


        db.collection("Students")
                .whereEqualTo("sid", sid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d(TAG, "name: "+document.getString("name"));
                                Log.d(TAG, "class: "+document.getString("class"));
                                TextView studentNameTV = findViewById(R.id.student_name);
                                TextView classNameTV = findViewById(R.id.class_name);
                                TextView rollnoTV = findViewById(R.id.rollno);
                                className = document.getString("class");
                                studentName = document.getString("name");
                                studentNameTV.setText("Student: "+studentName);
                                classNameTV.setText("Class: "+className);
                                rollnoTV.setText("Roll no: "+ sid);
                                getClassDetails(className);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


    }

    public void getClassDetails(String className) {
        db.collection("Class")
                .whereEqualTo("class", className)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Log.d(TAG, "name: "+document.getLong("students"));
                                HashMap<String, String> hm = (HashMap<String, String>) document.get("teachers");
                                StringBuilder teachersText = new StringBuilder();
                                for(String s : hm.keySet()) {
                                    teachersText.append(s + ":  " + hm.get(s) + "\n\n");
                                }
                                teachersTV.setText(teachersText.toString());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}