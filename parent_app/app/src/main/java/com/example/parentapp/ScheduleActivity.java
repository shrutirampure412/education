package com.example.parentapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class ScheduleActivity extends AppCompatActivity {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    final String TAG = "parents3 firebase";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        db.collection("Tasks")
                .whereEqualTo("day", "Wednesday")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                final HashMap<String, Map<String, String>> hm = (HashMap<String, Map<String, String>>) document.get("tasks");
                                TextView task1TV = findViewById(R.id.task1);
                                String task1 = hm.get("10-11").get("name");
                                task1TV.setText("10 am - 11 am : \n"+task1);

                                TextView task2TV = findViewById(R.id.task2);
                                String task2 = hm.get("11-12").get("name");
                                task2TV.setText("11 am - 12 pm : \n"+task2);

                                TextView task3TV = findViewById(R.id.task3);
                                String task3 = hm.get("12-13").get("name");
                                task3TV.setText("12 pm - 1 pm : \n"+task3);

                                TextView task4TV = findViewById(R.id.task4);
                                String task4 = hm.get("13-14").get("name");
                                task4TV.setText("1 pm - 2 pm : \n"+task4);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }
}