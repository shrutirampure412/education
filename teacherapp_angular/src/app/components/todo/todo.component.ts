import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { TmplAstElement } from '@angular/compiler';
import { TasksService } from 'src/app/services/tasks.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import {MatDialog} from '@angular/material/dialog';
import { DialogComponent } from 'src/app/dialog/dialog.component';
import { Task } from 'src/app/models/Task';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  task:string;
  link: string;
  af: AngularFirestore;
  todo: Task[] = [
    { link: '',  name:'Chemistry Lecture' },
    { link: '',  name:'Phiysics Test' },
    { link: '',  name:'SUPW' },
    { link: '',  name:'History Lecture' },

  ];
  
  // todo = [
  //   'Get to work',
  //   'Pick up groceries',
  //   'Go home',
  //   'Fall asleep'
  // ];

  

  monday:Task[] = [

  ];

  tuesday:Task[]= [
    { link: '',  name:'Math Fraction Test' },
    { link: '',  name:'Spell Bee Competition' },

  ];

  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    console.log(this.todo, this.monday, this.tuesday);
    }

    onClick(){
      //this.todo.push(this.task);
      this.todo.push({link:this.link, name:this.task});
      this.task = '';
      this.link = '';

    }
  constructor(public afs: AngularFirestore, public dialog:MatDialog) { 
    this.af = afs;
    
  }

  openDialog() {
    this.dialog.open(DialogComponent);
  }

  onUpdate(){
    var time = 11;
    
    let arr : Map<String, Task> = new Map<String, Task>();

    var len = this.monday.length;
    for (var i = 0; i < len; i++) {
        arr[time.toString().concat("-".concat((time+1).toString()))] = this.monday[i]
        time+=1;
    }

    this.af.collection('Tasks').add({
      day:"Monday",
      class:"4a",
      tasks:Object.assign({}, arr),
    })
    .then(function(docRef) {
     console.log("Document written with ID: ", docRef.id);
 })
 .catch(function(error) {
     console.error("Error adding document: ", error);
 });
this.openDialog();
  } 

  ngOnInit(): void {
  }

}
